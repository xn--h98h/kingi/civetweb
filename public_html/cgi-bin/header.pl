#!/usr/bin/perl -w
# $Id: header.pl,v 0.0 2013/04/08 03:23:52 michelc Exp $

$| = 1;
my $hacked =  0;
my $query = {};
if (exists $ENV{QUERY_STRING}) {
   my @params = split /\&/,$ENV{QUERY_STRING};
   foreach my $e (@params) {
      my ($p,$v) = split/=/,$e;
      $v =~ s/\+/ /g;
      $v =~ s/%(..)/chr(hex($1))/eg; # unhtml-ize (urldecoded)
      $query->{$p} = $v;
   }
}


# get script version (git commitid) :
my $hash = &get_digest('SHA1',sprintf("blob %u\0",(lstat(__FILE__))[7]),__FILE__);
my $gitid = lc substr($hash,0,7);
printf "X-VersionID: %s\n",$gitid;
printf "X-Source: %s\n",__FILE__;


if ($hacked) {
printf "HTTP/1.0 302 Moved\n";
printf "Location: https://headers.cloxy.net/?%s\n",$ENV{QUERY_STRING}
}
print "Content-Type: text/html, charset=UTF-8\r\n";
print "\r\n";




print <<"EOT";
<meta charset="utf-8"/>
<title>IPHu HEAD: view HTTP response header (curl -I)</title>
<h2>IPHu HEAD: View HTTP-Response Header (curl -I)</h2>

EOT
my $url = $query->{url} || shift || 'http://example.com/';

exit 0 unless $url;
#printf qq'<p>URL="<b><a href="%s">%s</a></b>"\n',$url,$url;
print qq'<form action=header.pl method="GET">\n';
printf qq'<p>URL: <input name="url" value="%s" placeholder="http://example.com"/></p>\n',$url;
print qq'</form>\n';

my $p = index($url,'://');
my $proto = substr($url,0,$p);
my ($addr,$uri) = split'/',substr($url,$p+3),2;
$uri = '' unless $uri;
my ($host,$port) = split ':',$addr;
if ($proto eq 'https' && $port eq '') {
  $port = 443;
} elsif ($proto eq 'http' && $port eq '') {
  $port = 80;
}

print "<pre>";
printf "X-proto: %s\n",$proto;
printf "X-addr: %s\n",$addr;
printf "X-host: %s\n",$host;
printf "X-port: %s\n",$port;
print "</pre>\n";

my $s;
if ($proto eq 'https') {
  use Net::HTTPS; # Make the request using Net::HTTPS.
  $s = Net::HTTPS->new(Host => "$host", 'PeerPort' => $port, 'HTTPVersion' => '1.0') || die $@;
} else {
  use Net::HTTP; # Make the request using Net::HTTP.
  $s = Net::HTTP->new(Host => "$host", 'PeerPort' => $port, 'HTTPVersion' => '1.0') || die $@;
}

print "<code><pre>";
printf "Request:\n%s.\n",$s->format_request(HEAD => "/$uri", 'User-Agent' => "Mozilla/5.0");
$s->write_request(HEAD => "/$uri", 'User-Agent' => "Mozilla/5.0");
print "Response:\n";
while(<$s>) { # raw headers.
    # Headers are done on a blank line.
    last unless m/\S/;
    print $_;
}
print "</pre></code>";




exit $?;

# -----------------------------------------------------
sub get_digest ($@) {
 my $alg = shift;
 my $nid = shift; # namespace id
 use Digest qw();
 local *F,
 open F,'<',$_[0]; binmode F; local $/ = undef;
 my $msg = Digest->new($alg) or die $!;
    $msg->add($nid);
    $msg->addfile(*F);
 close F;
 my $digest = uc( $msg->hexdigest() );
 return $digest; #hex form !
}
# -----------------------------------------------------
1;
